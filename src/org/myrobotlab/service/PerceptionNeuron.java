package org.myrobotlab.service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JOptionPane;

import org.myrobotlab.framework.Service;
import org.myrobotlab.framework.repo.ServiceType;
import org.myrobotlab.logging.Level;
import org.myrobotlab.logging.LoggerFactory;
import org.myrobotlab.logging.LoggingFactory;
import org.myrobotlab.perception.BvhDataHeader;
import org.myrobotlab.perception.CalcDataHeader;
import org.myrobotlab.perception.FramePerceptionListener;
import org.myrobotlab.perception.InMoovData;
import org.myrobotlab.perception.NeuronDataReaderLibrary;
import org.myrobotlab.perception.NeuronDataReaderLibrary.CalculationDataReceived;
import org.myrobotlab.perception.NeuronDataReaderLibrary.FrameDataReceived;
import org.slf4j.Logger;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.FloatByReference;


/**
 * TODO: Revisar como se conecta cuando hace establece el listener
 * @author diegocdl(at)galileo.edu
 *
 */
public class PerceptionNeuron extends Service implements FrameDataReceived {

	
	private static final long serialVersionUID = 1L;

	public final static Logger log = LoggerFactory.getLogger(PerceptionNeuron.class);
	
	transient List<FramePerceptionListener> listeners;
	
	private static String DEFAULT_IP 	= "127.0.0.1";
	private boolean connected 			= false;
	private static int DEFAULT_PORT 	= 7001;
	public long start_time = 0;
	public long count_frames = 0;
	
	private InMoovData inMoovData;
	public Pointer socket;
	public InMoov inMoov;
	
	public static int EXPECTED_DISPLACEMENT = 0;
	public static int EXPECTED_REFERENCE = 0;
	
	
	
	public PerceptionNeuron(String n) throws Exception {
		super(n);
		listeners = Collections.synchronizedList(new ArrayList<>());
		broadcastState();
	}
	
	public InMoovData getData() {
		return inMoovData;
	}

	/**
	 * Connect to the axis Neuron Software using TCP/IP using <code>ip=127.0.0.1</code> 
	 * @throws Exception if NeuronDataReader library is not found in the system
	 */
	public void connect() throws Exception {
		connect(DEFAULT_IP);
	}
	
	
	/**
	 * Register callback and connect to the axis Neuron Software using TCP/IP
	 * @param ip from the axis Neuron 
	 * @throws Exception if NeuronDataReader library is not found in the system 
	 */
	public void connect(String ip) throws Exception {
		if(!isConnected()) {
			
			// Register Callback and wait for avoid connection errors 
			try {
				NeuronDataReaderLibrary.BRRegisterFrameDataCallback(Pointer.NULL, this);
				Thread.sleep(2000);
			} catch(Exception e) {}
			
			try {
				socket = NeuronDataReaderLibrary.BRConnectTo(ip, DEFAULT_PORT);
				if(socket == null) {
					JOptionPane.showMessageDialog(null,"Perception Neuron can't connect to ip: "  + ip + " port: " + DEFAULT_PORT, "Alert", JOptionPane.WARNING_MESSAGE);
					throw new Exception("Perception Neuron can't connect to ip: "  + ip + " port: " + DEFAULT_PORT);
				}
				connected = true;
				start_time = Instant.now().getEpochSecond();
				broadcastState();
				
				int status = NeuronDataReaderLibrary.BRGetSocketStatus(socket);
				if( status == NeuronDataReaderLibrary.SocketStatus.CS_OffWork ) {
					JOptionPane.showMessageDialog(null, "Socket to Axis Neuron it's not working", "Error", JOptionPane.ERROR_MESSAGE);
					log.error("Socket to Axis Neuron it's not working");
				} else {
					log.info("Socket Status: " + getSocketStatus());
				}
				
			} catch(UnsatisfiedLinkError e) {
				JOptionPane.showMessageDialog(null, "Native Neuron Data Reader library not found", "Error", JOptionPane.ERROR_MESSAGE);
				throw new Exception("Native Neuron Data Reader library not found");
			} catch(NoClassDefFoundError e) {
				JOptionPane.showMessageDialog(null, "Native Neuron Data Reader library not found", "Error", JOptionPane.ERROR_MESSAGE);
				throw new Exception("Native Neuron Data Reader library not found");
			}
		} else {
			log.info("The perception Neuron Service it's already connected to Axis Neuron");
		}
			
	}
	
	
	/**
	 * Close the Connection with the Axis Neuron
	 */
	public void close() {
		try {
			if(socket != null) {
				NeuronDataReaderLibrary.BRCloseSocket(socket);
				connected = false;
				socket = null;
				broadcastState();
			} else {
				JOptionPane.showMessageDialog(null, "Error closing the Socket to the Axis Neuron", "Error", JOptionPane.ERROR_MESSAGE);
			}
		} catch(UnsatisfiedLinkError e) {
			JOptionPane.showMessageDialog(null, "Native Neuron Data Reader library not found", "Error", JOptionPane.ERROR_MESSAGE);	
		} catch(NoClassDefFoundError e) {
			JOptionPane.showMessageDialog(null, "Native Neuron Data Reader library not found", "Error", JOptionPane.ERROR_MESSAGE);
		}
	}
	
	
	/**
	 * Gets the Socket Status from the NeuronDataReaderLib
	 * @return String with the status
	 */
	public String getSocketStatus() {
		if(socket != null ) {
			int status = NeuronDataReaderLibrary.BRGetSocketStatus(socket);
			switch (status) {
			case NeuronDataReaderLibrary.SocketStatus.CS_OffWork:
				return "CS_OffWork";
			case NeuronDataReaderLibrary.SocketStatus.CS_Running:
				return "CS_Running";
			case NeuronDataReaderLibrary.SocketStatus.CS_Starting:
				return "CS_Starting";
			default:
				return "Not recognized " + status + ".";
			}
		} else {
			return "Not Connected";
		}
		
	}
	
	public void setInMoov(InMoov inMoov) {
		this.inMoov = inMoov;
	}
	
	/**
	 * This static method returns all the details of the class without
	 * it having to be constructed.  It has description, categories,
	 * dependencies, and peer definitions.
	 * 
	 * @return ServiceType - returns all the data
	 * 
	 */
	static public ServiceType getMetaData(){
		ServiceType meta = new ServiceType(PerceptionNeuron.class.getCanonicalName());
		meta.addDescription("Perception Neuron Service");
		meta.addCategory("sensor");		
		return meta;		
	}
	
	
	/**
	 * Add FrameUpdateListenner that will be called when a new frame is received
	 * @param listener
	 */
	public void addFrameUpdateListener(FramePerceptionListener listener) {
		synchronized(this.listeners){
			this.listeners.add(listener);
		}
	}
	
	/**
	 * Remove a FrameUpdateListener from the list of listeners
	 * @param listener
	 */
	public void removeFrameUpdateListener(FramePerceptionListener listener) {
		synchronized(this.listeners){
			for(int i = 0; i < this.listeners.size(); i++) {
				if(this.listeners.get(i) == listener) {
					this.listeners.remove(i);
				}
			}
		}
	}
	
	/**
	 * Check the connection with the Axis Neuron
	 * @return true if it's connected to the Axis Neuron
	 */
	public boolean isConnected() {
		return connected;
	}
	
	/**
	 * Check the Header of Bvh data stream
	 * Check  section 2.1.4. of the NeuronDataReader Runtime API   
	 * @param header
	 * @return
	 */
	public boolean validateBvhDataHeader(BvhDataHeader header) {
		int token1 = header.getPointer().getShort(0) & 0xFFFF;
		int dataVer = header.getPointer().getInt(2);
		int dataCount = header.getPointer().getShort(6) & 0xFFFF;
		int withDisp = header.getPointer().getByte(8);
		int withReference = header.getPointer().getByte(10);
		
		// TODO: remove this after tests
		System.out.println("Token1 : " + token1);
		System.out.println("Data count: " + dataCount);
		System.out.println("Displacement: " + withDisp);
		System.out.println("Reference: " + withReference);
		
		if( token1 != 0xDDFF )
			return false;
		
		if( withDisp != EXPECTED_DISPLACEMENT )
			return false;
		
		if( withReference != EXPECTED_REFERENCE )
			return false;
		
		return true;
	}
	
	/**
	 * This function handles the callback of Axis Neuron
	 * And notifies all the listeners.
	 */
	@Override
	public void apply(Pointer customedObj, Pointer sender, BvhDataHeader header, FloatByReference data) {
		// TODO: temporal solution for slow down the data rate
		if (java.lang.Thread.currentThread().getId() % 6 == 0 ) {
			int dataCount = header.getPointer().getShort(6); // leer dataCount
			
			if (validateBvhDataHeader(header)) {
				if(listeners != null) {
//		        	TODO: revisar si se compone el exception de error de acceso de memoria if(inMoovData != null) inMoovData.clear();
		        	synchronized(this.listeners){
		        		inMoovData = new InMoovData(data, dataCount);
			        	for(FramePerceptionListener l: listeners) {
			        		l.updatePosition(inMoovData);
			        	}
		        	}
		        	count_frames++;
		        	broadcastState();
		        }
				
//		        data.getPointer().clear(dataCount);
		        if(Runtime.getTotalMemory() - Runtime.getFreeMemory() > 1000000000) {
		        	log.error("Memoria usada: " + (Runtime.getTotalMemory() - Runtime.getFreeMemory()));
		        	Runtime.gc();
		        }
		        
			} else {
				log.error("Invalid Header received");
				
			}
	        	        
		}
		
	}

	
	
	public static void main(String[] args) {
		LoggingFactory.getInstance().configure();
		LoggingFactory.getInstance().setLevel(Level.INFO);
	}


	
}
