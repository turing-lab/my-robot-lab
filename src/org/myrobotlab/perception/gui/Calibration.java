/*
 * Created by JFormDesigner on Fri Oct 21 11:44:09 CST 2016
 */

package org.myrobotlab.perception.gui;

import java.awt.*;
import javax.swing.*;

import org.myrobotlab.service.InMoov;
import org.newdawn.slick.util.Log;

import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;

/**
 * @author diegocdl (at) galileo.edu
 */
public class Calibration extends JDialog {
	InMoov inMoov;
	
	public Calibration() throws Exception {
		this(null);
	}
	
	public Calibration(InMoov inMoov) throws Exception {
		this.inMoov = inMoov;
		initComponents();
	}

	private void initComponents() throws Exception {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - Diego Calderon
			panel1 = new HandCalibration();
			//======== this ========
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			setMinimumSize(new Dimension(400, 200));
			Container contentPane = getContentPane();
			contentPane.setLayout(new BorderLayout());
			contentPane.add(panel1, BorderLayout.CENTER);
			setLocationRelativeTo(getOwner());		
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - Diego Calderon
	private HandCalibration panel1;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
	

}
