/*
 * Created by JFormDesigner on Fri Oct 21 11:46:40 CST 2016
 */

package org.myrobotlab.perception.gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.*;
import javax.swing.table.*;

import org.bson.Document;
import org.myrobotlab.framework.Service;
import org.myrobotlab.math.Mapper;
import org.myrobotlab.perception.FramePerceptionListener;
import org.myrobotlab.perception.InMoovData;
import org.myrobotlab.service.InMoov;
import org.myrobotlab.service.InMoovArm;
import org.myrobotlab.service.InMoovHand;
import org.myrobotlab.service.PerceptionNeuron;
import org.myrobotlab.service.Runtime;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;



/**
 * @author diegocdl (at) galileo.edu
 */
public class HandCalibration extends JPanel implements FramePerceptionListener, ActionListener {
	
	private static final long serialVersionUID = 1L;
	
	public static final int WAITING 			= 0;
	public static final int OPEN_HAND 			= 1;
	public static final int CLOSE_HAND 			= 2;
	public static final int BICEP_EXTENDED 		= 3;
	public static final int BICEP_CONTRACTED 	= 4;
	public static final int OPEN_OMOPLATE 		= 5;
	public static final int CLOSE_OMOPLATE 		= 6;
	public static final int OPEN_ROTATE 		= 7;
	public static final int CLOSE_ROTATE 		= 8;
	public static final int FRONT_SHOULDER 		= 9;
	public static final int BACK_SHOULDER 		= 10;
	public static final int RESULT 				= 11;
	public static final int END					= 12;
	
	/** sample time in seconds **/
	public static final int SAMPLE_TIME = 5;
	public static final int FPS = 120;
	
	public int state = WAITING;
	public PerceptionNeuron pn;
	
	// The uiThread will change the states and display the UI for each state
	public Thread uiThread;

	// Reference to the InMoov to get the perception Neuron
	private InMoov inMoov;
	
	// Hash maps that will save the configuration for each motor
	public HashMap<String, MotorConfig> left;
	public HashMap<String, MotorConfig> right;
	
	// strings to identify each bone
	public static final String INDEX = "index";
	public static final String THUMB = "thumb";
	public static final String MIDDLE = "middle";
	public static final String RING = "ring";
	public static final String PINKY = "pinky";
	public static final String BICEP = "bicep";
	public static final String OMOPLATE = "omoplate";
	public static final String ROTATE = "rotate";
	public static final String SHOULDER = "shoulder";
	public static final String[] motorNames = new String[] {
		INDEX, THUMB, MIDDLE, RING, PINKY, BICEP, OMOPLATE, ROTATE, SHOULDER
	};
	
	public HandCalibration() throws Exception {
		this(null);
	}
	
	public HandCalibration(InMoov inMoov) throws Exception {
		this.inMoov = inMoov;
		initComponents();
		action_btn.addActionListener(this);
		
		if(inMoov == null)
			pn = (PerceptionNeuron)Runtime.getService("i01.perceptionNeuron");
		else 
			pn = inMoov.perceptionNeuron;
		
		// Subscribe this class to the perception Neuron Service
		pn.addFrameUpdateListener(this);
		pn.connect();
		
		// initialize the hash maps with empty Motor Configuration
		left 	= new HashMap<String, MotorConfig>();
		right 	= new HashMap<String, MotorConfig>();
		for(String s: motorNames) {
			left.put(s, new MotorConfig());
			right.put(s, new MotorConfig());
		}
	}

	private void initComponents() {
		// JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
		// Generated using JFormDesigner Evaluation license - Diego Calderon
		state_lbl = new JLabel();
		ready = new JLabel();
		progressBar1 = new JProgressBar();
		action_btn = new JButton();
		scrollPane1 = new JScrollPane();
		table1 = new JTable();

		//======== this ========
		setMinimumSize(new Dimension(400, 200));
		setPreferredSize(new Dimension(400, 200));
		setDoubleBuffered(false);

		// JFormDesigner evaluation mark
		setBorder(new javax.swing.border.CompoundBorder(
			new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
				"", javax.swing.border.TitledBorder.CENTER,
				javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
				java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));

		//---- state_lbl ----
		state_lbl.setText("Hand Calibration");
		state_lbl.setFont(new Font("Tahoma", Font.PLAIN, 20));
		add(state_lbl);

		//---- ready ----
		ready.setText("Ready: 0");
		ready.setFont(new Font("Tahoma", Font.PLAIN, 14));
		add(ready);
		add(progressBar1);

		//---- button1 ----
		action_btn.setText("Continue");
		add(action_btn);

		//======== scrollPane1 ========
		{

			//---- table1 ----
			table1.setModel(new DefaultTableModel(
				new Object[][] {

				},
				new String[] {
					"motor", "mo-cap from", "mo-cap to", "motor from", "motor to"
				}
			));
			scrollPane1.setViewportView(table1);
		}
		add(scrollPane1);
		// JFormDesigner - End of component initialization  //GEN-END:initComponents
	}

	// JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
	// Generated using JFormDesigner Evaluation license - Diego Calderon
	private JLabel state_lbl;
	private JLabel ready;
	private JProgressBar progressBar1;
	private JButton action_btn;
	private JScrollPane scrollPane1;
	private JTable table1;
	// JFormDesigner - End of variables declaration  //GEN-END:variables
	
	@Override
	public void updatePosition(InMoovData data) {
		Service.log.info("Update using: " + state);
		switch(state) {
			case WAITING: 
				//do nothing
				break;
			case OPEN_HAND:
				left.get(INDEX).from.add((int)data.getLeftHandIndexAngle());
				left.get(THUMB).from.add((int)data.getLeftHandThumbAngle());
				left.get(MIDDLE).from.add((int)data.getLeftHandMiddleAngle());
				left.get(RING).from.add((int)data.getLeftHandRingAngle());
				left.get(PINKY).from.add((int)data.getLeftHandPinkyAngle());
				
				right.get(INDEX).from.add((int)data.getRightHandIndexAngle());
				right.get(THUMB).from.add((int)data.getRightHandThumbAngle());
				right.get(MIDDLE).from.add((int)data.getRightHandMiddleAngle());
				right.get(RING).from.add((int)data.getRightHandRingAngle());
				right.get(PINKY).from.add((int)data.getRightHandPinkyAngle());
				break;
			case CLOSE_HAND:
				left.get(INDEX).to.add((int)data.getLeftHandIndexAngle());
				left.get(THUMB).to.add((int)data.getLeftHandThumbAngle());
				left.get(MIDDLE).to.add((int)data.getLeftHandMiddleAngle());
				left.get(RING).to.add((int)data.getLeftHandRingAngle());
				left.get(PINKY).to.add((int)data.getLeftHandPinkyAngle());

				right.get(INDEX).to.add((int)data.getRightHandIndexAngle());
				right.get(THUMB).to.add((int)data.getRightHandThumbAngle());
				right.get(MIDDLE).to.add((int)data.getRightHandMiddleAngle());
				right.get(RING).to.add((int)data.getRightHandRingAngle());
				right.get(PINKY).to.add((int)data.getRightHandPinkyAngle());
				break;
			case BICEP_EXTENDED:
				left.get(BICEP).from.add((int)data.getLeftBicepAngle());
				right.get(BICEP).from.add((int)data.getRightBicepAngle());
				break;
			case BICEP_CONTRACTED:
				left.get(BICEP).to.add((int)data.getLeftBicepAngle());
				right.get(BICEP).to.add((int)data.getRightBicepAngle());
				break;
			case OPEN_OMOPLATE:
				left.get(OMOPLATE).from.add((int)data.getLeftOmoplateAngle());
				right.get(OMOPLATE).from.add((int)data.getRightOmoplateAngle());
				break;
			case CLOSE_OMOPLATE:
				left.get(OMOPLATE).to.add((int)data.getLeftOmoplateAngle());
				right.get(OMOPLATE).to.add((int)data.getRightOmoplateAngle());
				break;
			case OPEN_ROTATE:
				left.get(ROTATE).from.add((int)data.getLeftRotateAngle());
				right.get(ROTATE).from.add((int)data.getRightRotateAngle());
				break;
			case CLOSE_ROTATE:
				left.get(ROTATE).to.add((int)data.getLeftRotateAngle());
				right.get(ROTATE).to.add((int)data.getRightRotateAngle());
				break;
			case FRONT_SHOULDER:
				left.get(SHOULDER).from.add((int)data.getLeftShoulderAngle());
				right.get(SHOULDER).from.add((int)data.getRightShoulderAngle());
				break;
			case BACK_SHOULDER:
				left.get(SHOULDER).to.add((int)data.getLeftShoulderAngle());
				right.get(SHOULDER).to.add((int)data.getRightShoulderAngle());
				state_lbl.setText("Back Shoulder");
				break;
			case RESULT:
				break;
		}
		
	}
	
	/**
	 * Set the progress Bar value to 0
	 */
	public void resetProgressBar() {
		progressBar1.setValue(0);
	}
	
	public void moveProgressBar() throws Exception {
		int totalTime = 5000; // 5 seconds
		int time = totalTime/100;
		for(int i =0; i <= 100; i++){
			progressBar1.setValue(i);
			Thread.sleep(time);
		}
		resetProgressBar();
	}
	
	public void timer() throws Exception {
		for(int i = 5; i >= 0; i--) {
			ready.setText("Ready: " + i);
			Thread.sleep(1000);
		}
	}
	
	public void updateUIWithState(int state) {
		this.state = state;
		//TODO: add pictures for each state that illustrate how the user have to put their hands/arms
		switch(state) {
			case WAITING: 
				// do nothing, just wait
				break;
			case OPEN_HAND:
				state_lbl.setText("Open Hand");
				break;
			case CLOSE_HAND:
				state_lbl.setText("Close Hand");
				break;
			case BICEP_EXTENDED:
				state_lbl.setText("Extend your bicep");
				break;
			case BICEP_CONTRACTED:
				state_lbl.setText("Contract your bicep");
				break;
			case OPEN_OMOPLATE:
				state_lbl.setText("Open Omoplate");
				break;
			case CLOSE_OMOPLATE:
				state_lbl.setText("Open Omoplate");
				break;
			case OPEN_ROTATE:
				state_lbl.setText("Open Rotate");
				break;
			case CLOSE_ROTATE:
				state_lbl.setText("Close Rotate");
				break;
			case FRONT_SHOULDER:
				state_lbl.setText("Front Shoulder");
				break;
			case BACK_SHOULDER:
				state_lbl.setText("Back Shoulder");
				break;
			case RESULT:
				pn.removeFrameUpdateListener(this);
				inMoov = (InMoov)Runtime.getService("i01");
				
				action_btn.setText("save");
				
				// if inMoov it's different from null update the mappers for each servo of the left hand
				if(inMoov != null) {
					InMoovHand leftHand = inMoov.hands.get(InMoov.LEFT);
					
					leftHand.index.mapper.setXValues(left.get(INDEX).averageFrom(), left.get(INDEX).averageTo());
					leftHand.thumb.mapper.setXValues(left.get(THUMB).averageFrom(), left.get(THUMB).averageTo());
					leftHand.majeure.mapper.setXValues(left.get(MIDDLE).averageFrom(), left.get(MIDDLE).averageTo());
					leftHand.ringFinger.mapper.setXValues(left.get(RING).averageFrom(), left.get(RING).averageTo());
					leftHand.pinky.mapper.setXValues(left.get(PINKY).averageFrom(), left.get(PINKY).averageTo());
				}
				
				state_lbl.setText("Results");
				ready.setVisible(false);
				
				// Make visible the table with the results
				DefaultTableModel model = (DefaultTableModel)table1.getModel();
				if(inMoov == null) {
					for(String s: motorNames){
						model.addRow(buildRow("left-" + s, left.get(s).averageFrom(), left.get(s).averageTo(), 0, 0));
						model.addRow(buildRow("right-" + s, right.get(s).averageFrom(), right.get(s).averageTo(), 0, 0));
					}
				} else {
					for(String s: motorNames){
						System.out.println(s);
						model.addRow(buildRow("left-" + s, left.get(s).averageFrom(), left.get(s).averageTo(), getMapperFromMotor("left", s).getMinY(), getMapperFromMotor("left", s).getMaxY()));
						model.addRow(buildRow("right-" + s, right.get(s).averageFrom(), right.get(s).averageTo(), getMapperFromMotor("right", s).getMinY(), getMapperFromMotor("right", s).getMaxY()));
					}
				}
				scrollPane1.setVisible(true);
				break;
			case END:
				state_lbl.setText("Information saved you can close the window now");
				action_btn.setVisible(false);
				scrollPane1.setVisible(false);
				break;
		}
	}
	
	public Mapper getMapperFromMotor(String side, String name) {		
		switch(name) {
			case INDEX: case THUMB: case MIDDLE: case RING: case PINKY:
				InMoovHand hand;
				if(side.equals("left")) {
					 hand = inMoov.hands.get(InMoov.LEFT);
				} else {
					 hand = inMoov.hands.get(InMoov.RIGHT);
				}
				if(hand != null) {
					switch(name) {
						case INDEX:
							return hand.index.mapper;
						case THUMB:
							return hand.thumb.mapper;
						case MIDDLE:
							return hand.majeure.mapper;
						case RING:
							return hand.ringFinger.mapper;
						case PINKY:
							return hand.pinky.mapper;
					}
				}
				
				break;
			case BICEP: case OMOPLATE: case ROTATE: case SHOULDER:
				InMoovArm arm;
				if(side.equals("left")) {
					arm = inMoov.getArms().get(InMoov.LEFT);
				} else {
					arm = inMoov.getArms().get(InMoov.RIGHT);
				}
				if(arm != null) {
					switch(name) {
						case BICEP:
							return arm.bicep.mapper;
						case OMOPLATE:
							return arm.omoplate.mapper;
						case SHOULDER:
							return arm.shoulder.mapper;
						case ROTATE:
							return arm.rotate.mapper;
					}
				}
				
				break;
		}
		return new Mapper(0,0,0,0);
	}
	
	public void saveDB(String username) {
		
		DBObject leftArm = new BasicDBObject();
		DBObject leftHand = new BasicDBObject();
		
		DBObject rightArm = new BasicDBObject();
		DBObject rightHand = new BasicDBObject();
		
		
		
		// update the config from table to model
		TableModel model =table1.getModel();
		for(int i = 0; i < model.getRowCount(); i++) {
			String name = (String)model.getValueAt(i, 0);
			String side = name.split("-")[0];
			name = name.split("-")[1];
			
			switch(name) {
				case INDEX: case THUMB: case MIDDLE: case RING: case PINKY:
					DBObject hand;
					if(side.equals("left")){
						hand = leftHand;
					} else {
						hand = rightHand;
					}
					hand.put(name, buildConfigObj((String)model.getValueAt(i, 1), (String)model.getValueAt(i, 2), (String)model.getValueAt(i, 3), (String)model.getValueAt(i, 4)));
				break;
				case BICEP: case ROTATE: case SHOULDER: case OMOPLATE:
					DBObject arm;
					if(side.equals("left")){
						arm = leftArm;
					} else {
						arm = rightArm;
					}
					arm.put(name, buildConfigObj((String)model.getValueAt(i, 1), (String)model.getValueAt(i, 2), (String)model.getValueAt(i, 3), (String)model.getValueAt(i, 4)));
				break;
			}
		}

		
		MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
		
		MongoDatabase db = mongoClient.getDatabase( "sp2" );
		System.out.println("Connect to database successfully");
			
		MongoCollection<Document> coll = db.getCollection("users");
		

		// remove the documents with the specified user name
		// that ensures only exists one document for one name
		coll.deleteOne(new Document("name", username));
		
		// insert the new document for that user name
		Document user = new Document();
		user.put("name", username);
		user.put("left-arm", leftArm);
		user.put("right-arm", rightArm);
		leftArm.put("hand", leftHand);
		rightArm.put("hand", rightHand);
		coll.insertOne(user);
		
		mongoClient.close();

	}
	
	
	public boolean checkDBConnection() {
		try {
			MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
			mongoClient.close();
			return true;
		} catch(Exception e) {
			return false;
		}
		
	}
	
	public DBObject buildConfigObj(String mocap_from, String mocap_to, String motor_from, String motor_to) {
		DBObject o = new BasicDBObject();
		o.put("motor-from", motor_from);
		o.put("motor-to", motor_to);
		o.put("mo-cap-from", mocap_from);
		o.put("mo-cap-to", mocap_to);
		return o;
	}

	
	public String[] buildRow(String name, int mocap_from, int mocap_to, double motor_from, double motor_to) {
		return new String[]{name, Integer.toString(mocap_from), Integer.toString(mocap_to), Integer.toString((int)motor_from), Integer.toString((int)motor_to)};
	}
	
	

	@Override
	public void actionPerformed(ActionEvent evt) {
		if(evt.getSource() == action_btn) {
			if(state == WAITING) {
				action_btn.setVisible(false);
				uiThread = new Thread(new Runnable() {
					@Override
					public void run() {
						try {
							updateUIWithState(OPEN_HAND);
							timer();
							moveProgressBar();
							
							updateUIWithState(CLOSE_HAND);
							timer();
							moveProgressBar();
							
							updateUIWithState(RESULT);
							action_btn.setVisible(true);
						} catch(Exception e) {
							e.printStackTrace();
						}
					}
				});
				
				uiThread.start();
			} else if(state == RESULT) {
				// Save profile in database
				String name = JOptionPane.showInputDialog(this, "Name: ", "");
				saveDB(name);
				
				
				updateUIWithState(END);
			}
			
		}
		
	}
	
	public class MotorConfig {
		int motorFrom;
		int motorTo;
		int mocapFrom;
		int mocapTo;
		ArrayList<Integer> from;
		ArrayList<Integer> to;
		
		public MotorConfig() {
			from = new ArrayList<Integer>(HandCalibration.FPS*HandCalibration.SAMPLE_TIME);
			to = new ArrayList<Integer>(HandCalibration.FPS*HandCalibration.SAMPLE_TIME);
		}
		
		public DBObject getConfigObj() {
			DBObject r = new BasicDBObject();
			r.put("motor-from", motorFrom);
			r.put("motor-to", motorTo);
			r.put("mo-cap-from", averageFrom());
			r.put("mo-cap-to", averageTo());
			return r;
		}

		public MotorConfig(int mtFrom, int mtTo, int mcFrom, int mcTo) {
			motorFrom = mtFrom;
			motorTo = mtTo;
			mocapFrom = mcFrom;
			mocapTo = mcTo;
		}
		
		protected int average(ArrayList<Integer> list) {
			if(list.size() == 0) return 0;
			int avg = 0;		
			for(Integer i: list ) {
				avg += i;
			}
			return avg/list.size();
		}
		
		public int averageFrom() {
			return average(from);
		}
		
		public int averageTo() {
			return average(to);
		}
		
		
	}
}
