package org.myrobotlab.perception;

import com.sun.jna.ptr.FloatByReference;

public class InMoovData {
	protected FloatByReference data;
	protected float[] dataArray;
	
	protected static final float Z_OFFSET = 180.0f;
	protected static final float Y_OFFSET = 80f;
	protected static final float X_OFFSET = 0f;
	
	protected static final int Y = 0;
	protected static final int X = 1;
	protected static final int Z = 2;
	
	// Body
	protected static final int HIPS_POSITION 		= 0;
	protected static final int HIPS 				= 1;
	protected static final int RIGHT_UP_LEG 		= 2;
	protected static final int RIGHT_LEG 			= 3;
	protected static final int RIGHT_FOOT 			= 4;
	protected static final int LEFT_UP_LEG 			= 5;
	protected static final int LEFT_LEG 			= 6;
	protected static final int LEFT_FOOT 			= 7;
	protected static final int SPINE 				= 8;
	protected static final int SPINE1				= 9;
	protected static final int SPINE2				= 10;
	protected static final int SPINE3				= 11;
	protected static final int NECK					= 12;
	protected static final int HEAD					= 13;
	protected static final int RIGHT_SHOULDER 		= 14;
	protected static final int RIGHT_ARM 			= 15;
	protected static final int RIGHT_FORE_ARM 		= 16;
	protected static final int RIGHT_HAND			= 17;
	
	// Right Fingers
	protected static final int RIGHT_HAND_THUMB1	= 18;
	protected static final int RIGHT_HAND_THUMB2	= 19;
	protected static final int RIGHT_HAND_THUMB3	= 20;
	protected static final int RIGHT_IN_HAND_INDEX	= 21;
	protected static final int RIGHT_HAND_INDEX1	= 22;
	protected static final int RIGHT_HAND_INDEX2	= 23;
	protected static final int RIGHT_HAND_INDEX3	= 24;
	protected static final int RIGHT_IN_HAND_MIDDLE	= 25;
	protected static final int RIGHT_HAND_MIDDLE1	= 26;
	protected static final int RIGHT_HAND_MIDDLE2	= 27;
	protected static final int RIGHT_HAND_MIDDLE3	= 28;
	protected static final int RIGHT_IN_HAND_RING	= 29;
	protected static final int RIGHT_HAND_RING1		= 30;
	protected static final int RIGHT_HAND_RING2		= 31;
	protected static final int RIGHT_HAND_RING3		= 32;
	protected static final int RIGHT_IN_HAND_PINKY	= 33;
	protected static final int RIGHT_HAND_PINKY1	= 34;
	protected static final int RIGHT_HAND_PINKY2	= 35;
	protected static final int RIGHT_HAND_PINKY3	= 36;
	
	// Body
	protected static final int LEFT_SHOULDER 		= 37;
	protected static final int LEFT_ARM 			= 38;
	protected static final int LEFT_FORE_ARM 		= 39;
	protected static final int LEFT_HAND			= 40;
	
	// Left Fingers
	protected static final int LEFT_HAND_THUMB1		= 41;
	protected static final int LEFT_HAND_THUMB2		= 42;
	protected static final int LEFT_HAND_THUMB3		= 43;
	protected static final int LEFT_IN_HAND_INDEX	= 44;
	protected static final int LEFT_HAND_INDEX1		= 45;
	protected static final int LEFT_HAND_INDEX2		= 46;
	protected static final int LEFT_HAND_INDEX3		= 47;
	protected static final int LEFT_IN_HAND_MIDDLE	= 48;
	protected static final int LEFT_HAND_MIDDLE1	= 49;
	protected static final int LEFT_HAND_MIDDLE2	= 50;
	protected static final int LEFT_HAND_MIDDLE3	= 51;
	protected static final int LEFT_IN_HAND_RING	= 52;
	protected static final int LEFT_HAND_RING1		= 53;
	protected static final int LEFT_HAND_RING2		= 54;
	protected static final int LEFT_HAND_RING3		= 55;
	protected static final int LEFT_IN_HAND_PINKY	= 56;
	protected static final int LEFT_HAND_PINKY1		= 57;
	protected static final int LEFT_HAND_PINKY2		= 58;
	protected static final int LEFT_HAND_PINKY3		= 59;
	
	protected int size;
	
	public InMoovData(FloatByReference data, int size) {
		this.data = data;
		this.size = size;
		this.dataArray = new float[size];
		System.out.println(size);
		// copy the data from the pointer
		for(int i = 0; i < size; i++) {
			
			dataArray[i] = this.data.getPointer().getFloat(i*4);
		}
		
//		clear();
	}
	
	protected void clear() {
		data.getPointer().clear(size);
	}
	
//	public int getOmoplateRotation(HashMap<String, BvhBone> bones){
//		int angle;
//		BvhBone bone = bones.get(LEFT_ARM);
//		angle = (int)bone.getXrotation();
//		return angle;
//	}
	
	public float getHipsXAngle() {
//		return data.getPointer().getFloat((HIPS*3 + Y)*4);
		return dataArray[HIPS*3 + Y];
	}
	
	// Left Arm
	public float getLeftOmoplateAngle() {
		// TODO: Check array position with the leftShoulder
//		return data.getPointer().getFloat((LEFT_ARM*3 + X)*4);
		return dataArray[LEFT_ARM*3 + X];
	}
	
	public float getLeftShoulderAngle() {
//		return data.getPointer().getFloat((LEFT_ARM*3 + X)*4);
		return dataArray[LEFT_ARM*3 + X];
	}
	
	public float getLeftRotateAngle() {
//		float value = data.getPointer().getFloat((LEFT_ARM*3 + Y)*4);
		float value = dataArray[LEFT_ARM*3 + Y];
		value = (value  + 30f*1.0f)/(- 89f + 21f)*(160f-90f)+ 90f;
		value = 180 - value - 20;
		if(value > 175) 
			value = 175;
		
		if(value < 90)
			value = 90;
		return value;
	}
	
	public float getLeftBicepAngle() {
//		float value = data.getPointer().getFloat((LEFT_FORE_ARM*3 + Y)*4);
		float value = dataArray[LEFT_FORE_ARM*3 + Y];
		value = Math.abs(value);
		if(value < 0) value = 0;
		if(value > 80) value = 80;
		return value;
	}
	
	
	// Right Arm
	public float getRightOmoplateAngle() {
//		return data.getPointer().getFloat((RIGHT_ARM*3 + X)*4);
		return dataArray[RIGHT_ARM*3 + X];
	}
	
	public float getRightShoulderAngle() {
//		return data.getPointer().getFloat((RIGHT_ARM*3 + X)*4);
		return dataArray[RIGHT_ARM*3 + X];
	}
	
	public float getRightRotateAngle() {
//		float value = data.getPointer().getFloat((RIGHT_ARM*3 + Y)*4);
		float value = dataArray[RIGHT_ARM*3 + Y];
		value = (value - 30f*1.0f)/(-42f -36f)*(160f-60f) + 50f;
		if(value > 160) 
			value = 160;
		
		if(value < 77)
			value =  77;
		return value;
	}
	
	public float getRightBicepAngle() {
//		float value = data.getPointer().getFloat((RIGHT_FORE_ARM*3 + Z)*4);
		float value = dataArray[RIGHT_FORE_ARM*3 + Z];
		value = Math.abs(value);
		if(value < 0) value = 0;
		if(value > 75) value = 75;
		return value;
	}
	
	
	// Left Hand
	public float getLeftHandThumbAngle() {
//		float value = data.getPointer().getFloat((LEFT_HAND_THUMB2*3 + Y)*4) + Y_OFFSET;
		float value = dataArray[LEFT_HAND_THUMB2*3 + Y];
//		value = Math.abs(value)*2f*1.4f;
		return value;
	}
	
	public float getLeftHandIndexAngle() {
//		float value = data.getPointer().getFloat((LEFT_HAND_INDEX1*3 + Z)*4) + Z_OFFSET;
		float value = dataArray[LEFT_HAND_INDEX1*3 + Z];
//		value = 170 - Math.abs(value)*2f;
		return value;
	}
	
	public float getLeftHandMiddleAngle() {
//		float value = data.getPointer().getFloat((LEFT_HAND_MIDDLE1*3 + Z)*4) + Z_OFFSET;
		float value = dataArray[LEFT_HAND_MIDDLE1*3 + Z];
//		value = Math.abs(value)*2;;
		return value;
	}
	
	public float getLeftHandRingAngle() {
//		float value = data.getPointer().getFloat((LEFT_HAND_RING1*3 + Z)*4) + Z_OFFSET;
		float value = dataArray[LEFT_HAND_RING1*3 + Z];
//		value = 150f - Math.abs(value)*2f;
		return value;
	}
	
	public float getLeftHandPinkyAngle() {
//		float value = data.getPointer().getFloat((LEFT_HAND_PINKY1*3 + Z)*4) + Z_OFFSET;
		float value = dataArray[LEFT_HAND_PINKY1*3 + Z];
//		value = Math.abs(value)*2f;
		return value;
	}
	
	public float getLeftWristAngle() {
//		return data.getPointer().getFloat((LEFT_FORE_ARM*3 + X)*4);
		return dataArray[LEFT_FORE_ARM*3 + X];
	}
	
	
	// Right Hand
	public float getRightHandThumbAngle() {
//		return data.getPointer().getFloat((RIGHT_HAND_THUMB1*3 + Y)*4);
		return dataArray[RIGHT_HAND_THUMB1*3 + Y];
	}
	
	public float getRightHandIndexAngle() {
//		return data.getPointer().getFloat((RIGHT_HAND_INDEX1*3 + Z)*4);
		return dataArray[RIGHT_HAND_INDEX1*3 + Z];
	}
	
	public float getRightHandMiddleAngle() {
//		return data.getPointer().getFloat((RIGHT_HAND_MIDDLE1*3 + Z)*4);
		return dataArray[RIGHT_HAND_MIDDLE1*3 + Z];
	}
	
	public float getRightHandRingAngle() {
//		return data.getPointer().getFloat((RIGHT_HAND_RING1*3 + Z)*4);
		return dataArray[RIGHT_HAND_RING1*3 + Z];
	}
	
	public float getRightHandPinkyAngle() {
//		return data.getPointer().getFloat((RIGHT_HAND_PINKY1*3 + Z)*4);
		return dataArray[RIGHT_HAND_PINKY1*3 + Z];
	}
	
	public float getRightWristAngle() {
//		return data.getPointer().getFloat((RIGHT_FORE_ARM*3 + X)*4);
		return dataArray[RIGHT_FORE_ARM*3 + X];
	}
}
