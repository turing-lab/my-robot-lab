package org.myrobotlab.perception;

public interface FramePerceptionListener {
	public void updatePosition(InMoovData data);
}
