/**
 *                    
 * @author diegocdl (at) galileo.edu
 *  
 * This file is part of MyRobotLab (http://myrobotlab.org).
 *
 * MyRobotLab is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version (subject to the "Classpath" exception
 * as provided in the LICENSE.txt file that accompanied this code).
 *
 * MyRobotLab is distributed in the hope that it will be useful or fun,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * All libraries in thirdParty bundle are subject to their own license
 * requirements - please refer to http://myrobotlab.org/libraries for 
 * details.
 * 
 * Enjoy !
 * 
 * */

package org.myrobotlab.control;

import java.awt.Dialog;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Instant;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import org.bson.Document;
import org.myrobotlab.logging.LoggerFactory;
import org.myrobotlab.perception.InMoovData;
import org.myrobotlab.perception.gui.Calibration;
import org.myrobotlab.service.GUIService;
import org.myrobotlab.service.InMoov;
import org.myrobotlab.service.InMoovArm;
import org.myrobotlab.service.InMoovHand;
import org.myrobotlab.service.PerceptionNeuron;
import org.myrobotlab.service.Runtime;
import org.newdawn.slick.util.Log;
import org.slf4j.Logger;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;


public class PerceptionNeuronGUI extends ServiceGUI implements ActionListener {
	
	
	private static final String IPADDRESS_PATTERN =
			"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	
	public final String db_host = "localhost";
	public final String db_name = "sp2";
	public final int db_port = 27017;
	
	static final long serialVersionUID = 1L;
	
	JButton btn_connect = new JButton("Connect");
	JButton btn_calibrate = new JButton("Calibrate");
	JLabel lbl_server = new JLabel("Server IP: ");
	JTextField txt_server = new JTextField("127.0.0.1");
	JLabel title = new JLabel("Perception Neuron");
	boolean isConnected = false;
	
	JLabel lbl_thumb 			= new JLabel("Thumb");
	JLabel lbl_index 			= new JLabel("Index");
	JLabel lbl_middle 			= new JLabel("Middle");
	JLabel lbl_ring 			= new JLabel("Ring");
	JLabel lbl_pinky			= new JLabel("Pinky");
	JLabel lbl_wrist			= new JLabel("Wrist");
	JLabel lbl_bicep			= new JLabel("Bicep");
	JLabel lbl_shoulder			= new JLabel("Shoulder");
	JLabel lbl_rotate			= new JLabel("Rotate");
	JLabel lbl_omoplate			= new JLabel("Omoplate");
	
	JSlider sld_rThumb 			= new JSlider(0, 180, 0);
	JSlider sld_rIndex 			= new JSlider(0, 180, 0);
	JSlider sld_rMiddle			= new JSlider(0, 180, 0);
	JSlider sld_rRing			= new JSlider(0, 180, 0);
	JSlider sld_rPinky			= new JSlider(0, 180, 0);
	JSlider sld_rWrist			= new JSlider(0, 180, 0);
	JSlider sld_rBicep			= new JSlider(0, 180, 0);
	JSlider sld_rShoulder		= new JSlider(0, 180, 0);
	JSlider sld_rRotate			= new JSlider(0, 180, 0);
	JSlider sld_rOmoplate		= new JSlider(0, 180, 0);
	
	JSlider sld_lThumb 			= new JSlider(0, 180, 0);
	JSlider sld_lIndex 			= new JSlider(0, 180, 0);
	JSlider sld_lMiddle			= new JSlider(0, 180, 0);
	JSlider sld_lRing			= new JSlider(0, 180, 0);
	JSlider sld_lPinky			= new JSlider(0, 180, 0);
	JSlider sld_lWrist			= new JSlider(0, 180, 0);
	JSlider sld_lBicep			= new JSlider(0, 180, 0);
	JSlider sld_lShoulder		= new JSlider(0, 180, 0);
	JSlider sld_lRotate			= new JSlider(0, 180, 0);
	JSlider sld_lOmoplate		= new JSlider(0, 180, 0);
	
	JLabel lbl_lThumbAngle 		= new JLabel("0 ");
	JLabel lbl_lIndexAngle 		= new JLabel("0 ");
	JLabel lbl_lMiddleAngle		= new JLabel("0 ");
	JLabel lbl_lRingAngle		= new JLabel("0 ");
	JLabel lbl_lPinkyAngle		= new JLabel("0 ");
	JLabel lbl_lWristAngle		= new JLabel("0 ");
	JLabel lbl_rBicepAngle		= new JLabel("0 ");
	JLabel lbl_rShoulderAngle	= new JLabel("0 ");
	JLabel lbl_rRotateAngle		= new JLabel("0 ");
	JLabel lbl_rOmoplateAngle	= new JLabel("0 ");

	JLabel lbl_rThumbAngle 		= new JLabel("0 ");
	JLabel lbl_rIndexAngle 		= new JLabel("0 ");
	JLabel lbl_rMiddleAngle		= new JLabel("0 ");
	JLabel lbl_rRingAngle		= new JLabel("0 ");
	JLabel lbl_rPinkyAngle		= new JLabel("0 ");
	JLabel lbl_rWristAngle		= new JLabel("0 ");
	JLabel lbl_lBicepAngle		= new JLabel("0 ");
	JLabel lbl_lShoulderAngle	= new JLabel("0 ");
	JLabel lbl_lRotateAngle		= new JLabel("0 ");
	JLabel lbl_lOmoplateAngle	= new JLabel("0 ");
	
	
	JComboBox<String> combo_user = new JComboBox<String>();
	JComboBox<String> combo_mode = new JComboBox<String>();
	private InMoov inMoov;
	
	protected static final String FULL = "full";
	protected static final String FULL_W_O_FINGERS = "full without fingers";
	protected static final String LEFT = "left";
	protected static final String LEFT_W_O_FINGERS = "left without fingers";
	protected static final String RIGHT = "right";
	protected static final String RIGHT_W_O_FINGERS = "left without fingers";
	
	
	
	public final static Logger log = LoggerFactory.getLogger(PerceptionNeuronGUI.class.getCanonicalName());

	public PerceptionNeuronGUI(final String boundServiceName, final GUIService myService, final JTabbedPane tabs) {
		super(boundServiceName, myService, tabs);
		readDataBase();
	}
	
	/**
	 * Connect to the database to get the saved user configurations
	 * and update the comboBox.
	 */
	public void readDataBase() {
		try {
			MongoClient mongoClient = new MongoClient( db_host , db_port );
			
			MongoDatabase db = mongoClient.getDatabase(db_name);
			Log.info("Connect to database successfully");
				
			MongoCollection<Document> coll = db.getCollection("users");
			Log.info("Collection users selected successfully");
				
			MongoCursor<Document> cursor = coll.find().iterator();
				
			while (cursor.hasNext()) { 
				Document e = cursor.next();
				System.out.println(e); 
				combo_user.addItem((String)e.get("name"));
			}
			combo_user.addActionListener(this);
			combo_user.setSelectedIndex(-1);
			mongoClient.close();
			
		} catch(Exception e) {
			Log.error("Can not connect with the mongo db server");
			JOptionPane.showMessageDialog(null, "Can not connect with the mongo db server", "Error", JOptionPane.ERROR_MESSAGE);
		}
		
	}
	
	/**
	 * Get the user with all configurations from the database using his name
	 * @param name of the user to get the configuration
	 * @return Document with all the configurations of the movements
	 */
	public Document getUser(String name) {
		Document user = null;
		try {
			MongoClient mongoClient = new MongoClient( "localhost" , 27017 );
			MongoDatabase db = mongoClient.getDatabase("sp2");				
			MongoCollection<Document> coll = db.getCollection("users");
			user = coll.find(new Document("name", name)).first();
			mongoClient.close();
		} catch(Exception e) {
			Log.error("Can not connect with the mongo db server");
			JOptionPane.showMessageDialog(null, "Can not connect with the mongo db server", "Error", JOptionPane.ERROR_MESSAGE);
		}
		return user;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// connect to Axis Neuron
		if(arg0.getSource() == btn_connect) {
			if(!isConnected) {
				String ip = txt_server.getText().trim();
				if(!ip.matches(IPADDRESS_PATTERN)) {
					JOptionPane.showMessageDialog(null, "Check the server ip", "Error", JOptionPane.ERROR_MESSAGE);
					Log.error("Check the server ip");
				} else {
					send("connect", ip);
				}
			} else {
				send("close");
			}
		} 
		
		if(arg0.getSource() == btn_calibrate) {
			try {
				log.info("creating modal window");
				if(inMoov == null) {
					// TODO: not always the inMoov service will be named i01
					inMoov = (InMoov)Runtime.getService("i01");
				}
				
				// remove temporarily listener from inMoov
				inMoov.perceptionNeuron.removeFrameUpdateListener(inMoov);
				Calibration calibrate = new Calibration(inMoov);
//				calibrate.setModalExclusionType(Dialog.ModalExclusionType.);
				calibrate.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
				calibrate.setVisible(true);
				
				// add the listener again 
				inMoov.perceptionNeuron.addFrameUpdateListener(inMoov);
			} catch(Exception e) {
				Log.error(e.getMessage());
			}		
		} 
		
		if(arg0.getSource() == combo_user) {
			// update mappers of InMoov using user's configuration stored in database
			String user_name = (String)combo_user.getSelectedItem();
			Document user = getUser(user_name);
			
			log.info("User: " + user_name + " was selected");
			if(user != null) {
				Document leftArm = (Document)user.get("left-arm");
				Document leftHand = (Document)leftArm.get("hand");
				
				Document rightArm = (Document)user.get("right-arm");
				Document rightHand = (Document)rightArm.get("hand");
				
				// ensure the existence of inMoov reference
				if(inMoov == null) {
					inMoov = (InMoov)Runtime.getService("i01");
				}
				
				if(inMoov != null) {
					if(combo_mode.getSelectedItem() == FULL || combo_mode.getSelectedItem() == LEFT) {
						InMoovHand inMoovHand = inMoov.hands.get(InMoov.LEFT);
						
						// set all fingers of left hand
						Document index = (Document)leftHand.get("index");
						inMoovHand.index.mapper.setXValues(index.get("mo-cap-from"), index.get("mo-cap-to"));
						inMoovHand.index.mapper.setYValues(index.get("motor-from"), index.get("motor-to"));
						
						Document thumb = (Document)leftHand.get("thumb");
						inMoovHand.thumb.mapper.setXValues(thumb.get("mo-cap-from"), thumb.get("mo-cap-to"));
						inMoovHand.thumb.mapper.setYValues(thumb.get("motor-from"), thumb.get("motor-to"));
						
						Document middle = (Document)leftHand.get("middle");
						inMoovHand.majeure.mapper.setXValues(middle.get("mo-cap-from"), middle.get("mo-cap-to"));
						inMoovHand.majeure.mapper.setYValues(middle.get("motor-from"), middle.get("motor-to"));
						
						Document ring = (Document)leftHand.get("ring");
						inMoovHand.ringFinger.mapper.setXValues(ring.get("mo-cap-from"), ring.get("mo-cap-to"));
						inMoovHand.ringFinger.mapper.setYValues(ring.get("motor-from"), ring.get("motor-to"));
						
						Document pinky = (Document)leftHand.get("pinky");
						inMoovHand.pinky.mapper.setXValues(pinky.get("mo-cap-from"), pinky.get("mo-cap-to"));
						inMoovHand.pinky.mapper.setYValues(pinky.get("motor-from"), pinky.get("motor-to"));
					}
					
					if(combo_mode.getSelectedItem() == FULL || combo_mode.getSelectedItem() == RIGHT) {
						InMoovHand inMoovHand = inMoov.hands.get(InMoov.RIGHT);
						
						// set all fingers of left hand
						Document index = (Document)rightHand.get("index");
						inMoovHand.index.mapper.setXValues(index.get("mo-cap-from"), index.get("mo-cap-to"));
						inMoovHand.index.mapper.setYValues(index.get("motor-from"), index.get("motor-to"));
						
						Document thumb = (Document)rightHand.get("thumb");
						inMoovHand.thumb.mapper.setXValues(thumb.get("mo-cap-from"), thumb.get("mo-cap-to"));
						inMoovHand.thumb.mapper.setYValues(thumb.get("motor-from"), thumb.get("motor-to"));
						
						Document middle = (Document)rightHand.get("middle");
						inMoovHand.majeure.mapper.setXValues(middle.get("mo-cap-from"), middle.get("mo-cap-to"));
						inMoovHand.majeure.mapper.setYValues(middle.get("motor-from"), middle.get("motor-to"));
						
						Document ring = (Document)rightHand.get("ring");
						inMoovHand.ringFinger.mapper.setXValues(ring.get("mo-cap-from"), ring.get("mo-cap-to"));
						inMoovHand.ringFinger.mapper.setYValues(ring.get("motor-from"), ring.get("motor-to"));
						
						Document pinky = (Document)rightHand.get("pinky");
						inMoovHand.pinky.mapper.setXValues(pinky.get("mo-cap-from"), pinky.get("mo-cap-to"));
						inMoovHand.pinky.mapper.setYValues(pinky.get("motor-from"), pinky.get("motor-to"));
					}
					
					// set left arm
					if(combo_mode.getSelectedItem() == FULL || combo_mode.getSelectedItem() == LEFT || combo_mode.getSelectedItem() == LEFT_W_O_FINGERS) {
						Document bicep = (Document)leftArm.get("bicep");
						inMoov.leftArm.getBicep().mapper.setXValues(bicep.get("mo-cap-from"), bicep.get("mo-cap-to"));
						inMoov.leftArm.getBicep().mapper.setYValues(bicep.get("motor-from"), bicep.get("motor-to"));
						
						Document omoplate = (Document)leftArm.get("omoplate");
						inMoov.leftArm.getOmoplate().mapper.setXValues(omoplate.get("mo-cap-from"), omoplate.get("mo-cap-to"));
						inMoov.leftArm.getOmoplate().mapper.setYValues(omoplate.get("motor-from"), omoplate.get("motor-to"));
						
						Document rotate = (Document)leftArm.get("rotate");
						inMoov.leftArm.getRotate().mapper.setXValues(rotate.get("mo-cap-from"), rotate.get("mo-cap-to"));
						inMoov.leftArm.getRotate().mapper.setYValues(rotate.get("motor-from"), rotate.get("motor-to"));
						
						Document shoulder = (Document)leftArm.get("shoulder");
						inMoov.leftArm.getShoulder().mapper.setXValues(shoulder.get("mo-cap-from"), shoulder.get("mo-cap-to"));
						inMoov.leftArm.getShoulder().mapper.setYValues(shoulder.get("motor-from"), shoulder.get("motor-to"));
					}
					
					// set right arm
					if(combo_mode.getSelectedItem() == FULL || combo_mode.getSelectedItem() == RIGHT || combo_mode.getSelectedItem() == RIGHT_W_O_FINGERS) {
						Document bicep = (Document)rightArm.get("bicep");
						inMoov.rightArm.getBicep().mapper.setXValues(bicep.get("mo-cap-from"), bicep.get("mo-cap-to"));
						inMoov.rightArm.getBicep().mapper.setYValues(bicep.get("motor-from"), bicep.get("motor-to"));
						
						Document omoplate = (Document)leftArm.get("omoplate");
						inMoov.rightArm.getOmoplate().mapper.setXValues(omoplate.get("mo-cap-from"), omoplate.get("mo-cap-to"));
						inMoov.rightArm.getOmoplate().mapper.setYValues(omoplate.get("motor-from"), omoplate.get("motor-to"));
						
						Document rotate = (Document)leftArm.get("rotate");
						inMoov.rightArm.getRotate().mapper.setXValues(rotate.get("mo-cap-from"), rotate.get("mo-cap-to"));
						inMoov.rightArm.getRotate().mapper.setYValues(rotate.get("motor-from"), rotate.get("motor-to"));
						
						Document shoulder = (Document)leftArm.get("shoulder");
						inMoov.rightArm.getShoulder().mapper.setXValues(shoulder.get("mo-cap-from"), shoulder.get("mo-cap-to"));
						inMoov.rightArm.getShoulder().mapper.setYValues(shoulder.get("motor-from"), shoulder.get("motor-to"));
					}
				}	
			} else {
				log.error("User configuration is not found in the database");
				// TODO: update the combo box with the current information from database
			}
		}
	}

	@Override
	public void attachGUI() {
		// commented out subscription due to this class being used for
		// un-defined gui's

		 subscribe("publishState", "getState", PerceptionNeuron.class);
		// send("publishState");
	}

	@Override
	public void detachGUI() {
		// commented out subscription due to this class being used for
		// un-defined gui's
		 unsubscribe("publishState", "getState", PerceptionNeuron.class);
	}
	
	public void guiConnect() {
		btn_connect.setText("disconnect");
		btn_calibrate.setEnabled(false);
		txt_server.setEnabled(false);
		combo_user.setEnabled(false);
	}
	
	public void guiDisconnect() {
		btn_connect.setText("connect");
		btn_calibrate.setEnabled(true);
		txt_server.setEnabled(true);
		combo_user.setEnabled(true);
	}

	public void getState(PerceptionNeuron pn) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				if(inMoov == null)
					inMoov = pn.inMoov;
				if(pn.isConnected()) {
					guiConnect();
					isConnected = true;
				} else {
					guiDisconnect();
					isConnected = false;
				}
				
				InMoovData data = pn.getData();
				InMoovHand left_hand = inMoov.hands.get(InMoov.LEFT);
				InMoovHand right_hand = inMoov.hands.get(InMoov.RIGHT);
				InMoovArm left_arm = inMoov.leftArm;
				InMoovArm right_arm = inMoov.rightArm;
				int temp;
				if(data != null) {
					// Thumb
					temp = left_hand.thumb.mapper.calcInt(data.getLeftHandThumbAngle());
					lbl_lThumbAngle.setText(temp + " ");
					sld_lThumb.setValue(temp);
					
					temp = right_hand.thumb.mapper.calcInt(data.getRightHandThumbAngle());
					lbl_rThumbAngle.setText(temp + " ");
					sld_rThumb.setValue(temp);
					
					// Index
					temp = left_hand.index.mapper.calcInt(data.getLeftHandIndexAngle());
					lbl_lIndexAngle.setText(temp + " ");
					sld_lIndex.setValue(temp);
					
					temp = right_hand.index.mapper.calcInt(data.getRightHandIndexAngle());
					lbl_rIndexAngle.setText(temp + " ");
					sld_rIndex.setValue(temp);
					
					// Middle
					temp = left_hand.majeure.mapper.calcInt(data.getLeftHandMiddleAngle());
					lbl_lMiddleAngle.setText(temp + " ");
					sld_lMiddle.setValue(temp);
					
					temp = right_hand.majeure.mapper.calcInt(data.getRightHandMiddleAngle());
					lbl_rMiddleAngle.setText(temp + " ");
					sld_rMiddle.setValue(temp);
					
					// Ring 
					temp = left_hand.ringFinger.mapper.calcInt(data.getLeftHandRingAngle());
					lbl_lRingAngle.setText(temp + " ");
					sld_lRing.setValue(temp);
					
					temp = right_hand.ringFinger.mapper.calcInt(data.getRightHandRingAngle());
					lbl_rRingAngle.setText(temp + " ");
					sld_rRing.setValue(temp);
					
					// Pinky  
					temp = left_hand.pinky.mapper.calcInt(data.getLeftHandPinkyAngle());
					lbl_lPinkyAngle.setText(temp + " ");					
					sld_lPinky.setValue(temp);
					
					temp = right_hand.pinky.mapper.calcInt(data.getRightHandPinkyAngle());
					lbl_rPinkyAngle.setText(temp + " ");
					sld_rPinky.setValue(temp);
					
					// Wrist  
					temp = left_hand.wrist.mapper.calcInt(data.getLeftWristAngle());
					lbl_lWristAngle.setText(temp + " ");
					sld_lWrist.setValue(temp);
					
					temp = right_hand.wrist.mapper.calcInt(data.getRightWristAngle());
					lbl_rWristAngle.setText(temp + " ");
					sld_rWrist.setValue(temp);
					
					// Bicep
					temp = left_arm.bicep.mapper.calcInt(data.getLeftBicepAngle());
					lbl_lBicepAngle.setText((int)data.getLeftBicepAngle() + " ");
					sld_lBicep.setValue((int)data.getLeftBicepAngle());
					
					temp = right_arm.bicep.mapper.calcInt(data.getRightBicepAngle());
					lbl_rBicepAngle.setText(temp + " ");
					sld_rBicep.setValue(temp);
					
					// Shoulder
					temp = left_arm.shoulder.mapper.calcInt(data.getLeftShoulderAngle());
					lbl_lShoulderAngle.setText(temp + " ");
					sld_lShoulder.setValue(temp);
					
					temp = right_arm.shoulder.mapper.calcInt(data.getRightShoulderAngle());
					lbl_rShoulderAngle.setText(temp + " ");
					sld_rShoulder.setValue(temp);
					
					// Rotate
					temp = left_arm.rotate.mapper.calcInt(data.getLeftRotateAngle());
					lbl_lRotateAngle.setText(temp + " ");
					sld_lRotate.setValue(temp);
					
					temp = right_arm.rotate.mapper.calcInt(data.getRightRotateAngle());
					lbl_rRotateAngle.setText(temp + " ");
					sld_rRotate.setValue(temp);
					
					// Omoplate
					temp = left_arm.omoplate.mapper.calcInt(data.getLeftOmoplateAngle());
					lbl_lOmoplateAngle.setText(temp + " ");
					sld_lOmoplate.setValue(temp);
					
					temp = right_arm.omoplate.mapper.calcInt(data.getRightOmoplateAngle());
					lbl_rOmoplateAngle.setText(temp + " ");
					sld_rOmoplate.setValue(temp);
				}

				// update frames per second 
				long seconds = (Instant.now().getEpochSecond() - pn.start_time);
				if(seconds != 0) {
					long fps = pn.count_frames/seconds;
					System.out.println(fps + " fps");
				}
				
				
			}
		});
	}

	/**
	 * Initialize all the GUI elements
	 */
	@Override
	public void init() {
		JPanel main_panel = new JPanel();
		main_panel.setLayout(new BoxLayout(main_panel, BoxLayout.Y_AXIS));
		JPanel control_panel = new JPanel();
		control_panel.setLayout(new FlowLayout());
		lbl_server.setLabelFor(txt_server);
		control_panel.add(lbl_server);
		control_panel.add(txt_server);
		control_panel.add(btn_connect);
		control_panel.add(btn_calibrate);
		control_panel.add(combo_user);
		main_panel.add(combo_mode);
		
		// fill modes
		combo_mode.addItem(FULL);
		combo_mode.addItem(FULL_W_O_FINGERS);
		combo_mode.addItem(LEFT);
		combo_mode.addItem(LEFT_W_O_FINGERS);
		combo_mode.addItem(RIGHT);
		combo_mode.addItem(RIGHT_W_O_FINGERS);
		combo_mode.setSelectedItem(LEFT);
		
		JPanel motors_panel = new JPanel();
		GridLayout gl = new GridLayout(0, 5, 0, 0);
		
		motors_panel.setLayout(gl);
		
		// Thumb 
		motors_panel.add(lbl_thumb);
		motors_panel.add(sld_lThumb);
		motors_panel.add(lbl_lThumbAngle);
		motors_panel.add(sld_rThumb);
		motors_panel.add(lbl_rThumbAngle);
		
		// Index
		motors_panel.add(lbl_index);
		motors_panel.add(sld_lIndex);
		motors_panel.add(lbl_lIndexAngle);
		motors_panel.add(sld_rIndex);
		motors_panel.add(lbl_rIndexAngle);
		
		// Middle
		motors_panel.add(lbl_middle);
		motors_panel.add(sld_lMiddle);
		motors_panel.add(lbl_lMiddleAngle);
		motors_panel.add(sld_rMiddle);
		motors_panel.add(lbl_rMiddleAngle);
		
		// Pinky
		motors_panel.add(lbl_pinky);
		motors_panel.add(sld_lPinky);
		motors_panel.add(lbl_lPinkyAngle);
		motors_panel.add(sld_rPinky);
		motors_panel.add(lbl_rPinkyAngle);
		
		// wrist
		motors_panel.add(lbl_wrist);
		motors_panel.add(sld_lWrist);
		motors_panel.add(lbl_lWristAngle);
		motors_panel.add(sld_rWrist);
		motors_panel.add(lbl_rWristAngle);
		
		// Bicep
		motors_panel.add(lbl_bicep);
		motors_panel.add(sld_lBicep);
		motors_panel.add(lbl_lBicepAngle);
		motors_panel.add(sld_rBicep);
		motors_panel.add(lbl_rBicepAngle);
		
		// Shoulder
		motors_panel.add(lbl_shoulder);
		motors_panel.add(sld_lShoulder);
		motors_panel.add(lbl_lShoulderAngle);
		motors_panel.add(sld_rShoulder);
		motors_panel.add(lbl_rShoulderAngle);
		
		// Omoplate
		motors_panel.add(lbl_omoplate);
		motors_panel.add(sld_lOmoplate);
		motors_panel.add(lbl_lOmoplateAngle);
		motors_panel.add(sld_rOmoplate);
		motors_panel.add(lbl_rOmoplateAngle);
		
		// Rotate
		motors_panel.add(lbl_rotate);
		motors_panel.add(sld_lRotate);
		motors_panel.add(lbl_lRotateAngle);
		motors_panel.add(sld_rRotate);
		motors_panel.add(lbl_rRotateAngle);
		
		main_panel.add(control_panel);
		main_panel.add(motors_panel);
		
		btn_connect.addActionListener(this);
		btn_calibrate.addActionListener(this);
			
		display.setLayout(new GridLayout(0, 1));
		display.add(main_panel);
	}

}
